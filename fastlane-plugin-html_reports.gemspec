# coding: utf-8

lib = File.expand_path("../lib", __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'fastlane/plugin/html_reports/version'

Gem::Specification.new do |spec|
  spec.name          = 'fastlane-plugin-html_reports'
  spec.version       = Fastlane::HtmlReports::VERSION
  spec.author        = 'Alexander Semenov'
  spec.email         = 'asemenovboyarka@gmail.com'

  spec.summary       = 'Set of acitons, allows gather results of screengrab/snapshot execution into nuseful html report'
  # spec.homepage      = "https://github.com/<GITHUB_USERNAME>/fastlane-plugin-html_reports"
  spec.license       = "MIT"

  spec.files         = Dir["lib/**/*"] + %w[README.md LICENSE]
  spec.test_files    = spec.files.grep(%r{^(test|spec|features)/})
  spec.require_paths = ['lib']

  spec.add_dependency 'fastimage', '> 1.6'

  spec.add_development_dependency 'pry'
  spec.add_development_dependency 'bundler'
  spec.add_development_dependency 'rspec'
  spec.add_development_dependency 'rake'
  spec.add_development_dependency 'rubocop'
  spec.add_development_dependency 'fastlane', '>= 1.95.0'
end
