require 'erb'
require 'json'

module Fastlane
  module Actions
    module SharedValues
    end

    # To share this integration with the other fastlane users:
    # - Fork https://github.com/KrauseFx/fastlane
    # - Clone the forked repository
    # - Move this integration into lib/fastlane/actions
    # - Commit, push and submit the pull request

    class SnapshotMultiReportAction < Action
      def self.run(params)
        # action is intented to run from plugin fastlane-plugin-html_reports and use its assets
        @lib_path = File.join(Helper.gem_path('fastlane-plugin-html_reports'), "lib")

        # fastlane will take care of reading in the parameter and fetching the environment variable:
        @screenshots_path = params[:screenshots_path]
        @language_top_dir = params[:language_top_dir]

        top_level_subdirs = Dir["#{@screenshots_path}/*/"].map { |a| File.basename(a) }
        # we assume all languages has screenshots for all simulators, so it would be sufficient to check only first language}
        second_level_subdirs = Dir["#{@screenshots_path}/#{top_level_subdirs[0]}/*/"].map { |a| File.basename(a) }

        if @language_top_dir
          @languages = top_level_subdirs
          @devices = second_level_subdirs
        else
          @languages = second_level_subdirs
          @devices = top_level_subdirs
        end
        @title = params[:html_title]

        # if user have provided html template - use it, fallback to default one ohterwise
        if params[:html_template_path].to_s != ''
          html_template_path = params[:html_template_path]
        else
          template_filename = @language_top_dir ? "screenshots_list.erb" : "screenshots_list_no_lang.erb"
          html_template_path = File.join(@lib_path, "assets/#{template_filename}")
        end
        template = File.read(html_template_path)

        result = ERB.new(template).result(binding)
        File.open(@screenshots_path + '/screenshots.html', 'w') do |fo|
          fo.write(result)
        end

        ### collecting metadata for screenshot compare script
        filenames = {}

        orientations = %w[portrait landscape]

        other_orientation = []
        if @language_top_dir
          metadata_screenshots_path = "#{@screenshots_path}/#{@languages[0]}/#{@devices[0]}/#{@languages[0]}"
        else
          metadata_screenshots_path = "#{@screenshots_path}/#{@devices[0]}/#{@languages[0]}"
        end
        Dir[File.join(metadata_screenshots_path, "*.png")].sort.each do |fn|
          filename = File.basename(fn)
          added = false

          orientations.each do |o|
            next unless filename.include? o
            filenames[o] ||= []
            filenames[o] << filename
            added = true
            break
          end
          other_orientation << filename unless added
        end

        orientations.delete_if { |orientation| filenames.key?(orientation) == false }
        # if we couldn't detect orientaion - put all that screenshots in 'other' category
        unless other_orientation.empty?
          filenames['other'] = other_orientation
          orientations << 'other'
        end

        metadata_json = {
          'devices' => @devices,
          'languages' => @languages,
          'orientations' => orientations,
          'filenames' => filenames
        }

        self.generate_compare_page(metadata_json)
      end

      def self.generate_compare_page(metadata)
        # save metadata for future use
        File.open(@screenshots_path + '/metadata.json', 'w') do |f|
          f.write(metadata.to_json)
        end

        @screenshots_metadata = metadata.to_json
        if @language_top_dir
          @screenshot_path_pattern = "lang + '/' + device +'/' + lang + '/' + filename"
        else
          @screenshot_path_pattern = "device + '/' + lang +'/' + filename"
        end

        ### writing html for compare from assets
        compare_template = File.read(File.join(@lib_path, "assets/screenshots_compare.erb"))
        compare_result = ERB.new(compare_template).result(binding)

        File.open(@screenshots_path + '/screenshots_compare.html', 'w') do |fo|
          fo.write(compare_result)
        end
      end

      #####################################################
      # @!group Documentation
      #####################################################

      def self.description
        'Gathers all language/device html reports to single html table '
      end

      def self.details
        # Optional:
        # this is your change to provide a more detailed description of this action
        'Action iterates over given screenshots folder for each language and each device type, picks eachs screenshots.html file and creates gathered table to show all of it. Also this adds metadata and html/js page for screenshots comparison'
      end

      def self.available_options
        # Define all options your action supports.

        # Below a few examples
        [
          FastlaneCore::ConfigItem.new(key: :screenshots_path,
                                       env_name: 'FL_SNAPSHOT_MULTI_REPORT_SCREENSHOTS_PATH', # The name of the environment variable
                                       description: 'Base directory for screenshots, from where action will gather single html reports, iterating through language dirs', # a short description of this parameter
                                       verify_block: proc do |value|
                                         raise "No screenshots directory for SnapshotMultiReportAction given, pass using `screenshots_path: 'path/'`".red unless value && !value.empty?
                                       end),
          FastlaneCore::ConfigItem.new(key: :html_title,
                                       env_name: 'FL_SNAPSHOT_MULTI_REPORT_HTML_TITLE', # The name of the environment variable
                                       description: 'Title to show in result html', # a short description of this parameter
                                       default_value: 'Screenshots report'),
          FastlaneCore::ConfigItem.new(key: :html_template_path,
                                       env_name: 'FL_SNAPSHOT_MULTI_REPORT_HTML_TEMPLATE_PATH', # The name of the environment variable
                                       description: 'Path to ERB template for HTML report', # a short description of this parameter
                                       default_value: '',
                                       optional: true),
          FastlaneCore::ConfigItem.new(key: :language_top_dir,
                                       env_name: 'FL_SNAPSHOT_LANGUAGE_TOP_DIR', # The name of the environment variable
                                       description: 'Should actions thread top dirs as languages. If true, dir structure expected like this "<lang>/<device>/<lang>", otherwise - <device>/<lang> ', # a short description of this parameter
                                       default_value: true,
                                       is_string: false)
        ]
      end

      def self.output; end

      def self.return_value
        # If you method provides a return value, you can describe here what it does
      end

      def self.authors
        # So no one will ever forget your contribution to fastlane :) You are awesome btw!
        ['SemenovAlexander']
      end

      def self.is_supported?(platform)
        # you can do things like
        #
        #  true
        #
        #  platform == :ios
        #
        #  [:ios, :mac].include? platform
        #

        %i[ios mac android].include? platform
      end
    end
  end
end
