module Fastlane
  module Helper
    class HtmlReportsHelper
      # class methods that you define here become available in your action
      # as `Helper::HtmlReportsHelper.your_method`
      #
      def self.show_message
        UI.message("Hello from the html_reports plugin helper!")
      end
    end
  end
end
